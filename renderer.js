let app = require('electron').remote;
let dialog = app.dialog;
let visualizer = require('./visualizer');
let $ = require('jquery');
let Player = require('audio-player');

let player = new Player();
player.audio.addEventListener("timeupdate", function () {
    if (player.audio.duration !== 0) {
        progress.val(player.audio.currentTime / player.audio.duration * 100);
    }
});

let playBtn = $("#play-btn");
let progress = $("#progress");
let playIcon = $("#play-btn-icon");
playBtn.click(() => {
    if (currentFile) {
        if (player.playing) {
            player.pause(currentFile);
            playIcon.addClass("fa-play");
            playIcon.removeClass("fa-stop");
            setTimeout(function () {
                progress.val(0)
            }, 100);
        } else {
            player.play(currentFile);
            playIcon.addClass("fa-stop");
            playIcon.removeClass("fa-play");
        }
    }
});
let ampThreshold = $("#amp-threshold");
let zcrThreshold = $("#zcr-threshold");
let toleranceThreshold = $("#tolerance-threshold");

let zoomHorizontal = $("#zoom-h-btn");
zoomHorizontal.click(() => {
    visualizer.updateSize(visualizer.canvas.width * 1.2, visualizer.canvas.height);
    visualizer.detectSilence(parseFloat(ampThreshold.val()), parseFloat(zcrThreshold.val()));

});
let zoomOutHorizontal = $("#zoom-out-h-btn");
zoomOutHorizontal.click(() => {
    visualizer.updateSize(visualizer.canvas.width / 1.2, visualizer.canvas.height);
    visualizer.detectSilence(parseFloat(ampThreshold.val()), parseFloat(zcrThreshold.val()));

});
let zoomVertical = $("#zoom-v-btn");
zoomVertical.click(()=> {
    visualizer.updateSize(visualizer.canvas.width, visualizer.canvas.height * 1.2);
    visualizer.detectSilence(parseFloat(ampThreshold.val()), parseFloat(zcrThreshold.val()));
});
let zoomOutVertical = $("#zoom-out-v-btn");
zoomOutVertical.click(()=> {
    visualizer.updateSize(visualizer.canvas.width, visualizer.canvas.height / 1.2);
    visualizer.detectSilence(parseFloat(ampThreshold.val()), parseFloat(zcrThreshold.val()));
});

let audioData = null;
let silenceDetector = $("#silence-detect-btn");
let silencePositions = null;
let silencePositionsElem = $("#silence-positions");
silenceDetector.click(()=> {
    if (!audioData) {
        return
    }
    console.log(parseInt(toleranceThreshold.val()));
    let silenceData = visualizer.detectSilence(parseFloat(ampThreshold.val()), parseFloat(zcrThreshold.val()), parseInt(toleranceThreshold.val()), true);
    if (!silenceData) {
        return
    }
    silencePositions = [];
    for (let c = 0; c < audioData.numberOfChannels; c++) {
        let result = silenceData.result[c];
        let windows = silenceData.windows[c];
        silencePositions.push([]);
        for (let i = 0; i < result.length; i++) {
            let begin = windows[result[i][0]].begin;
            let end = windows[windows.length - 1].end;
            if (windows[result[i][1]]) {
                end = windows[result[i][1]].end;
            }

            silencePositions[c].push([
                Math.round(begin / audioData.sampleRate * 1000) / 1000,
                Math.round(end / audioData.sampleRate * 1000) / 1000
            ])
        }
    }
    silencePositionsElem.html("");
    for (let c = 0; c < silencePositions.length; c++) {
        silencePositionsElem.append('<p class="file-name button"><span class="icon is-small"><i class="fa fa-arrow-right"></i></span><span>Channel ' + (c + 1) + '</span></p>')
        for (let i = 0; i < silencePositions[c].length; i++) {
            let e = $($.parseHTML('<div><span>' + silencePositions[c][i][0] + '</span>' +
                '<span class="icon is-small"><i class="fa fa-arrow-circle-right"></i></span>' +
                '<span>' + silencePositions[c][i][1] + '</span>' +
                '</div>'));
            e.hover(()=> {
                visualizer.showHighlightRect(c, i);
            });
            silencePositionsElem.append(e);
        }
    }

});

let zoomReset = $("#zoom-reset-btn");
zoomReset.click(()=> {
    console.log("wtf");

    visualizer.resetSize();
    visualizer.detectSilence(parseFloat(ampThreshold.val()), parseFloat(zcrThreshold.val()));
});

let openFile = $("#open-file-btn");
let currentFile = null;
let fileFormatBox = $("#file-format");

openFile.click(() => {

    dialog.showOpenDialog(function (fileNames) {
        // fileNames is an array that contains all the selected
        if (fileNames === undefined) {
            console.log("No file selected");
        } else {
            visualizer.readFile(fileNames[0], function (data) {
                fileFormatBox.html("");
                let format = data.format;
                console.log(data);
                audioData = data;
                fileFormatBox.append("<div>Channels: " + format.numberOfChannels + "</div>");
                fileFormatBox.append("<div>Sample Rate: " + format.sampleRate + "</div>");
                fileFormatBox.append("<div>Byte Rate: " + format.byteRate + "</div>");
                fileFormatBox.append("<div>Bits Per Sample: " + format.bitDepth + "</div>");
                audioData.duration = data.channelData[0].length / data.sampleRate;
                fileFormatBox.append("<div>Duration: " + Math.round(audioData.duration * 1000) / 1000 + "s</div>");
                silencePositions = null;
                silencePositionsElem.html("");
            });
            currentFile = fileNames[0];
            $("#file-name").html(currentFile.split('/').pop());
        }
    });

});


