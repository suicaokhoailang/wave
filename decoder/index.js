"use strict";

let formats = {
    0x0001: "lpcm",
    0x0003: "lpcm"
};

const decode =(buffer)=> {
    if (global.Buffer && buffer instanceof global.Buffer) {
        buffer = Uint8Array.from(buffer).buffer;
    }

    return new Promise(function(resolve, reject) {
        let dataView = new DataView(buffer);
        let reader = getHeaderDecoder(dataView);

        if (reader.string(4) !== "RIFF") {
            return reject(new TypeError("RIFF Expected."));
        }

        reader.uint32();

        if (reader.string(4) !== "WAVE") {
            return reject(new TypeError("WAVE Expected."));
        }

        let format = null;
        let audioData = null;

        do {
            let chunkType = reader.string(4);
            let chunkSize = reader.uint32();

            switch (chunkType) {
                case "fmt ":
                    format = decodeHeader(reader, chunkSize);
                    if (format instanceof Error) {
                        return reject(format);
                    }
                    break;
                case "data":
                    audioData = decodeData(reader, chunkSize, format);
                    if (audioData instanceof Error) {
                        return reject(format);
                    }
                    break;
                default:
                    reader.skip(chunkSize);
                    break;
            }
        } while (audioData === null);
        audioData.format = format;
        resolve(audioData);
    });
}

const decodeHeader = (headerDecoder, chunkSize) =>{
    let formatId = headerDecoder.uint16();

    if (!formats.hasOwnProperty(formatId)) {
        return new TypeError("Unsupported format in WAV file: 0x" + formatId.toString(16));
    }

    let format = {
        formatId: formatId,
        floatingPoint: formatId === 0x0003,
        numberOfChannels: headerDecoder.uint16(),
        sampleRate: headerDecoder.uint32(),
        byteRate: headerDecoder.uint32(),
        blockSize: headerDecoder.uint16(),
        bitDepth: headerDecoder.uint16()
    };
    headerDecoder.skip(chunkSize - 16);

    return format;
}

const decodeData = (headerDecoder, chunkSize, format)=> {
    let length = Math.floor(chunkSize / format.blockSize);
    let numberOfChannels = format.numberOfChannels;
    let sampleRate = format.sampleRate;
    let channelData = new Array(numberOfChannels);

    for (let ch = 0; ch < numberOfChannels; ch++) {
        channelData[ch] = new Float32Array(length);
    }

    let retVal = readPCM(headerDecoder, channelData, length, format);

    if (retVal instanceof Error) {
        return retVal;
    }

    return {
        numberOfChannels: numberOfChannels,
        length: length,
        sampleRate: sampleRate,
        channelData: channelData
    };
}

const readPCM = (reader, channelData, length, format) =>{
    let bitDepth = format.bitDepth;
    let floatingPoint = format.floatingPoint ? "f" : "";
    let methodName = "pcm" + bitDepth + floatingPoint;

    if (!reader[methodName]) {
        return new TypeError("Not supported bit depth: " + format.bitDepth);
    }

    let read = reader[methodName].bind(reader);
    let numberOfChannels = format.numberOfChannels;

    for (let i = 0; i < length; i++) {
        for (let ch = 0; ch < numberOfChannels; ch++) {
            channelData[ch][i] = read();
        }
    }

    return null;
}

const getHeaderDecoder = (dataView) =>{
    let currentPosition = 0;

    return {
        skip: function(n) {
            currentPosition += n;
        },
        uint8: function() {
            let data = dataView.getUint8(currentPosition, true);

            currentPosition += 1;

            return data;
        },
        int16: function() {
            let data = dataView.getInt16(currentPosition, true);

            currentPosition += 2;

            return data;
        },
        uint16: function() {
            let data = dataView.getUint16(currentPosition, true);

            currentPosition += 2;

            return data;
        },
        uint32: function() {
            let data = dataView.getUint32(currentPosition, true);

            currentPosition += 4;

            return data;
        },
        string: function(n) {
            let data = "";

            for (let i = 0; i < n; i++) {
                data += String.fromCharCode(this.uint8());
            }

            return data;
        },
        pcm8: function() {
            let data = dataView.getUint8(currentPosition) - 128;

            currentPosition += 1;

            return data < 0 ? data / 128 : data / 127;
        },
        pcm16: function() {
            let data = dataView.getInt16(currentPosition, true);

            currentPosition += 2;

            return data < 0 ? data / 32768 : data / 32767;
        },
        pcm24: function() {
            let x0 = dataView.getUint8(currentPosition + 0);
            let x1 = dataView.getUint8(currentPosition + 1);
            let x2 = dataView.getUint8(currentPosition + 2);
            let xx = (x0 + (x1 << 8) + (x2 << 16));
            let data = xx > 0x800000 ? xx - 0x1000000 : xx;

            currentPosition += 3;

            return data < 0 ? data / 8388608 : data / 8388607;
        },
        pcm32: function() {
            let data = dataView.getInt32(currentPosition, true);

            currentPosition += 4;

            return data < 0 ? data / 2147483648 : data / 2147483647;
        },
        pcm32f: function() {
            let data = dataView.getFloat32(currentPosition, true);

            currentPosition += 4;

            return data;
        },
        pcm64f: function() {
            let data = dataView.getFloat64(currentPosition, true);

            currentPosition += 8;

            return data;
        }
    };
}

module.exports.decode = decode;
