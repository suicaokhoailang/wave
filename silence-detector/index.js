let windowDuration = 0.02;//20ms

module.exports.detect = (data, ampThreshold,zcrThreshold, toleranceThreshold)=> {
    console.log("threshold: ",ampThreshold,zcrThreshold,toleranceThreshold);
    let duration = data.channelData[0].length/data.sampleRate;
    let nWindows = duration/windowDuration;
    //window width
    let width = data.sampleRate*windowDuration;
    //store check result for each window
    let windows = [];
    //iterate through all windows
    for(let c = 0; c < data.numberOfChannels; c++){
        windows.push([]);
        for (let i = 0.0; i < data.channelData[c].length; i += width) {
            //begin and end indices of each window
            let windowBegin = i;
            let windowEnd = i + width;
            if(i + width >= data.channelData[c].length){
                windowEnd = data.channelData[c].length - 1;
            }
            //find averageAmp
            let averageAmp = 0;
            let nZeroCrossing = 0;
            for(let j = windowBegin; j < windowEnd;j++){
                averageAmp += Math.abs(data.channelData[c][j]);
                if(data.channelData[c][j + 1] && data.channelData[c][j]*data.channelData[c][j + 1] < 0){
                    nZeroCrossing++;
                }
            }
            averageAmp /= (windowEnd-windowBegin);
            let zeroCrossingRate = nZeroCrossing/(width)*data.sampleRate;
            //store check result
            windows[c].push({
                silence: averageAmp < ampThreshold && zeroCrossingRate < zcrThreshold,
                begin: windowBegin,
                end: windowEnd,
                averageAmp: averageAmp,
                zcr: zeroCrossingRate
            });
        }
    }


    let result = [];
    for(let c = 0; c < data.numberOfChannels; c++){
        result.push([]);
        let i = 0;
        let tolerance  = 0;
        while (i < windows[c].length){
            if(windows[c][i].silence || tolerance < toleranceThreshold){
                let j = i + 1;
                tolerance = 0;
                while (  j < windows[c].length && (windows[c][j].silence || tolerance < toleranceThreshold)){
                    if(!windows[c][j].silence){
                        tolerance++;
                    }else{
                        tolerance = 0;
                    }
                    j++;
                }
                if(j < windows[c].length){
                    j-= tolerance + 1;
                }
                result[c].push([i,j]);
                i = j + 1;
            }else{
                i++;
            }
        }
    }
    // for(let i = 0; i < nWindows;i++){
    //     console.log(windows[0][i].zcr, windows[0][i].silence);
    // }
    return {
        windows: windows,
        result: result
    };
};