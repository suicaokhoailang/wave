"use strict";
const fabric = require('fabric').fabric;
const fs = require('fs');
const silenceDetector = require('../silence-detector');
let canvas = new fabric.Canvas("wave");
const initialWidth = 1120;
const initialHeight = 520;

const initialAmp = 0.005;
const initialZcr = 900;
const initialTolerance= 5;

canvas.setHeight(initialHeight);
canvas.setWidth(initialWidth);
let silenceDetectorOn = false;

let audioData = null;
let silencePath = null;
let zcrPath = null;
const WavDecoder = require('wav-decoder');
const readFile = (filePath) => {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, (err, buffer) => {
            if (err) {
                return reject(err);
            }
            return resolve(buffer);
        });
    });
};


const visualize = () => {
    let halfHeight = canvas.height / 2 / audioData.numberOfChannels;
    let channelData = audioData.channelData;
    let pathDescription = '';
    let pixelPerPoint = canvas.width / channelData[0].length;
    let drawData = [];
    if (pixelPerPoint >= 0.25) {
        for (let c = 0; c < audioData.numberOfChannels; c++) {
            drawData.push([]);
            drawData[c] = Array.prototype.slice.call(channelData[c]);
        }
    }
    else {
        //need resampling to fit screen
        let step = 1 / pixelPerPoint / 8;
        for (let c = 0; c < audioData.numberOfChannels; c++) {
            drawData.push([]);
            for (let i = 0.0; i < channelData[c].length; i += step) {
                drawData[c].push(channelData[c][Math.floor(i)]);
            }
        }
        pixelPerPoint = 0.125;

    }
    for (let c = 0; c < audioData.numberOfChannels; c++) {
        if (drawData[c].length == Math.floor(canvas.width / pixelPerPoint) + 1) {
            drawData[c].splice(drawData[c].length - 1, 1)
        }
        pathDescription += 'M 0 ' + (halfHeight + c * canvas.height / 2);
        for (let i = 0; i < drawData[c].length - 1; i += 2) {
            let beginX = pixelPerPoint * i, endX = pixelPerPoint * (i + 1);
            let beginY = (drawData[c][i] * halfHeight) + halfHeight + c * canvas.height / 2;
            let endY = (drawData[c][i + 1] * halfHeight) + halfHeight + c * canvas.height / 2;
            pathDescription += 'L ' + beginX + ' ' + beginY + ' ' + ' L ' + endX + ' ' + endY;
        }
    }


    let path = new fabric.Path(pathDescription);
    path.set({fill: 'rgba(0,0,0,0)', stroke: 'green', antiAlias: true,opacity: 0.75});
    path.lockMovementX = true;
    path.lockMovementY = true;
    path.lockRotation = true;
    path.lockScalingX = true;
    path.lockScalingY = true;
    path.lockSkewingX = true;
    path.lockSkewingY = true;

    //Drawing Y axis
    let yAxisDescription = 'M 0 0 L 0 ' +canvas.height;
    for(let i = 0; i <  audioData.numberOfChannels*8;i++){
        yAxisDescription += 'M 0 ' + i*canvas.height/(audioData.numberOfChannels*8) + ' L '+canvas.width+' ' + i*canvas.height/(audioData.numberOfChannels*8);

    }



    let yAxisPath = new fabric.Path(yAxisDescription);
    yAxisPath.set({fill: 'rgba(0,0,0,0)', stroke: 'white', opacity: 1, strokeWidth: 1});
    yAxisPath.lockMovementX = true;
    yAxisPath.lockMovementY = true;
    yAxisPath.lockRotation = true;
    yAxisPath.lockScalingX = true;
    yAxisPath.lockScalingY = true;
    yAxisPath.lockSkewingX = true;
    yAxisPath.lockSkewingY = true;
    canvas.add(yAxisPath);

    //Drawing X axis
    let xPos = canvas.height - 3;
    let xAxisDescription = 'M 0 ' + xPos + ' L ' + canvas.width + ' ' + xPos;
    audioData.duration = audioData.channelData[0].length / audioData.sampleRate;

    for (let i = 1; i < 17; i++) {
        xAxisDescription += 'M ' + i * canvas.width / 17 + ' ' + xPos + ' L ' + i * canvas.width / 17 + ' 0' ;
        let txt = Math.round(i * audioData.duration / 17 * 100) / 100 + "";
        canvas.add(new fabric.Text(txt, {
            left: i * canvas.width / 17 + 5,
            top: xPos - 25,
            fontSize: 12,
            fontFamily: "Ubuntu",
            fill: "white"
        }));
    }

    let xAxisPath = new fabric.Path(xAxisDescription);
    xAxisPath.set({fill: 'rgba(0,0,0,0)', stroke: 'white', opacity: 1, strokeWidth: 1});
    canvas.add(xAxisPath);
    canvas.add(path);
    canvas.renderAll();

};

module.exports.canvas = canvas;
module.exports.readFile = function (fileName, callback) {
    //current data exist
    if (audioData != null) {
        canvas.clear();
        silenceDetectorOn = false;
    }
    readFile(fileName).then((buffer) => {
        return WavDecoder.decode(buffer);
    }).then(function (data) {
        audioData = data;
        for (let c = 0; c < audioData.numberOfChannels; c++) {
            let channelData = audioData.channelData[c];
            for (let i = 0; i < channelData.length; i++) {
                channelData[i] = -channelData[i];
            }
        }

        if (callback) {
            callback(data);
        }
        visualize(data);
    });
};
module.exports.resetSize = () => {
    if (!audioData) {
        return
    }
    module.exports.updateSize(initialWidth, initialHeight);
};
module.exports.updateSize = (w, h) => {
    if (!audioData || w > 32000 || Math.round(w) < initialWidth || Math.round(h) < initialHeight) {
        return
    }

    w = Math.floor(w);
    h = Math.floor(h);
    canvas.setHeight(h);
    canvas.setWidth(w);
    canvas.clear();
    visualize();
};
let highlightRects = null;
let currentRect = null;
module.exports.detectSilence = (ampThreshold, zcrThreshold, toleranceThreshold,toggle) => {
    if (!audioData) {
        return
    }
    if (toggle) {
        silenceDetectorOn = !silenceDetectorOn;
    }
    if (!silenceDetectorOn) {
        if (silencePath != null) {
            canvas.remove(silencePath);
        }
        if (currentRect) {
            canvas.remove(currentRect)
        }
        if(zcrPath){
            canvas.remove(zcrPath);
        }
        canvas.renderAll();
        return
    }

    if (ampThreshold !== 0 && !ampThreshold || isNaN(ampThreshold)) {
        ampThreshold = initialAmp;
    }
    if (zcrThreshold !== 0 && !zcrThreshold || isNaN(zcrThreshold)) {
        zcrThreshold = initialZcr;
    }
    if (toleranceThreshold !== 0 && !toleranceThreshold || isNaN(toleranceThreshold)) {
        toleranceThreshold = initialTolerance;
    }
    let pathDescription = '';
    let zcrPathDescription = '';
    let silenceData = silenceDetector.detect(audioData, ampThreshold, zcrThreshold,toleranceThreshold);
    let pixelPerPoint = canvas.width / audioData.channelData[0].length;
    let y = canvas.height / audioData.numberOfChannels;
    highlightRects = [];
    for (let c = 0; c < audioData.numberOfChannels; c++) {
        highlightRects.push([]);

        let result = silenceData.result[c];
        let windows = silenceData.windows[c];

        for (let i = 0; i < windows.length; i++) {
            let begin = windows[i].begin;
            let end = windows[i].end;
            if (i == 0) {
                zcrPathDescription += 'M ' + (begin + end) * pixelPerPoint / 2 + ' ' + (y * (c + 1 / 2 - windows[i].zcr / audioData.sampleRate / 2) );
            } else {
                zcrPathDescription += 'L ' + (begin + end) * pixelPerPoint / 2 + ' ' + (y * (c + 1 / 2 - windows[i].zcr / audioData.sampleRate / 2) );
            }
        }
        for (let i = 0; i < result.length; i++) {
            let begin = windows[result[i][0]].begin;
            let end = windows[windows.length - 1].end;
            if (windows[result[i][1]]) {
                end = windows[result[i][1]].end;
            }
            pathDescription += 'M ' + (begin * pixelPerPoint) + ' ' + (c * canvas.height / 2);
            pathDescription += ' L ' + (end * pixelPerPoint) + ' ' + (c * canvas.height / 2);
            pathDescription += ' L ' + (end * pixelPerPoint) + ' ' + (y + c * canvas.height / 2);
            pathDescription += ' L ' + (begin * pixelPerPoint) + ' ' + (y + c * canvas.height / 2);
            let r = new fabric.Rect({
                left: begin * pixelPerPoint,
                top: c * canvas.height / 2,
                width: (end - begin) * pixelPerPoint,
                height: y,
                fill: 'red',
                opacity: 0.5,
                strokeWidth: 1
            });
            highlightRects[c].push(r);
            r.selectable = false;
        }

    }

    if (silencePath != null) {
        canvas.remove(silencePath);
    }
    silencePath = new fabric.Path(pathDescription);
    silencePath.set({fill: '#888', opacity: 0.5, strokeWidth: 1});
    silencePath.selectable = false;

    zcrPath = new fabric.Path(zcrPathDescription);
    zcrPath.set({stroke: 'orange', opacity: 1, strokeWidth: 2, fill: 'rgba(0,0,0,0)'});
    zcrPath.selectable = false;

    canvas.add(silencePath);
    canvas.add(zcrPath);

    canvas.renderAll();
    return silenceData;
};

module.exports.showHighlightRect = function (channel, index) {
    if (!highlightRects) {
        return
    }
    let rect = highlightRects[channel][index];
    if (currentRect) {
        canvas.remove(currentRect);
    }
    canvas.add(rect);
    currentRect = rect;
};