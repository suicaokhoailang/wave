"use strict";
const fabric = require('fabric').fabric;
const fs = require('fs');
const silenceDetector = require('../silence-detector');
const PIXI = require('pixi.js');


let pixiCanvas = document.getElementById('wave');
var renderer = PIXI.autoDetectRenderer(1120, 500, {view: pixiCanvas,antialias: true});
let stage = new PIXI.Container();
stage.interactive = true;
let graphics = new PIXI.Graphics();

let silenceDetectorOn = false;

let audioData = null;
let silencePath = null;

const WavDecoder = require('wav-decoder');
const readFile = (filePath) => {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, (err, buffer) => {
            if (err) {
                return reject(err);
            }
            return resolve(buffer);
        });
    });
};


const visualize = ()=> {
    let halfHeight = renderer.view.height / 2 / audioData.numberOfChannels;
    let channelData = audioData.channelData;
    let pathDescription = '';
    let pixelPerPoint = renderer.view.width / channelData[0].length;
    let drawData = [];
    if (pixelPerPoint >= 0.125) {
        for (let c = 0; c < audioData.numberOfChannels; c++) {
            drawData.push([]);
            drawData[c] = Array.prototype.slice.call(channelData[c]);
        }
    }
    else {
        //need resampling to fit screen
        let step = 1 / pixelPerPoint / 8;
        for (let c = 0; c < audioData.numberOfChannels; c++) {
            drawData.push([]);
            for (let i = 0.0; i < channelData[c].length; i += step) {
                drawData[c].push(channelData[c][Math.floor(i)]);
            }
        }
        pixelPerPoint = 0.125;

    }
    graphics.clear();
    renderer.backgroundColor = 0xeeeeee;
    graphics.beginFill(0xFF3300, 0);
    graphics.lineStyle(1, 0x008000, 0.5);

    for (let c = 0; c < audioData.numberOfChannels; c++) {
        if (drawData[c].length == Math.floor(renderer.view.width / pixelPerPoint) + 1) {
            drawData[c].splice(drawData[c].length - 1, 1)
        }
        // pathDescription += 'M 0 ' + (halfHeight + c * canvas.height / 2);

        graphics.moveTo(0, halfHeight + c * renderer.view.height / 2);
        for (let i = 0; i < drawData[c].length - 1; i += 2) {

            //using fabric
            var beginX = pixelPerPoint * i, endX = pixelPerPoint * (i + 1);
            var beginY = (drawData[c][i] * halfHeight) + halfHeight + c * renderer.view.height / 2;
            var endY = (drawData[c][i + 1] * halfHeight) + halfHeight + c * renderer.view.height / 2;
            graphics.lineTo(beginX, beginY);
            graphics.lineTo(endX, endY);

        }
    }
    graphics.endFill();
    stage.addChild(graphics);
    animate();

    function animate() {

        renderer.render(stage);
        requestAnimationFrame(animate);
    }
};

module.exports.canvas = renderer.view;
module.exports.readFile = function (fileName) {
    //current data exist
    if (audioData != null) {
        graphics.clear();
        silenceDetectorOn = false;
    }
    readFile(fileName).then((buffer) => {
        return WavDecoder.decode(buffer);
    }).then(function (data) {
        audioData = data;
        let channelData = audioData.channelData[0];
        for (let i = 0; i < channelData.length; i++) {
            channelData[i] = -channelData[i];
        }
        visualize(data);
    });
};
module.exports.resetSize = ()=> {
    if (!audioData) {
        return
    }
    module.exports.updateSize(1120, 500);
};
module.exports.updateSize = (w, h) => {
    if (!audioData) {
        return
    }
    w = Math.floor(w);
    h = Math.floor(h);
    renderer.view.height = h;
    renderer.view.width = w;
    renderer.view.style.width = w + 'px';
    renderer.view.style.height = h + 'px';
    renderer.resize(w,h);
    visualize();
};
module.exports.detectSilence = (threshold, toggle)=> {
    return
    if (!audioData) {
        return
    }
    if (toggle) {
        silenceDetectorOn = !silenceDetectorOn;
    }
    if (!silenceDetectorOn) {
        if (silencePath != null) {
            canvas.remove(silencePath);
        }
        canvas.renderAll();
        return
    }
    if (!threshold) {
        threshold = 0.005;
    }
    let pathDescription = '';
    let silenceData = silenceDetector.detect(audioData, threshold);
    let pixelPerPoint = canvas.width / silenceData.nWindows;
    let y = canvas.height / audioData.numberOfChannels;
    for (let c = 0; c < audioData.numberOfChannels; c++) {
        for (let i = 0; i < silenceData.result[c].length; i++) {
            let s = silenceData.result[c][i];
            // pathDescription += 'M '+s[0]+' '+canvas.height/2;
            // pathDescription += ' L '+s[1]+' '+canvas.height/2;
            pathDescription += 'M ' + (s[0] * pixelPerPoint) + ' ' + (c * canvas.height / 2);
            pathDescription += ' L ' + (s[1] * pixelPerPoint) + ' ' + (c * canvas.height / 2);
            pathDescription += ' L ' + (s[1] * pixelPerPoint) + ' ' + (y + c * canvas.height / 2);
            pathDescription += ' L ' + (s[0] * pixelPerPoint) + ' ' + (y + c * canvas.height / 2);
            pathDescription += ' z';
        }
        console.log(pathDescription);
    }

    if (silencePath != null) {
        canvas.remove(silencePath);
    }
    silencePath = new fabric.Path(pathDescription);
    silencePath.set({fill: 'red', opacity: 0.5, strokeWidth: 4});
    silencePath.lockMovementX = true;
    silencePath.lockMovementY = true;
    silencePath.lockRotation = true;
    silencePath.lockScalingX = true;
    silencePath.lockScalingY = true;
    silencePath.lockSkewingX = true;
    silencePath.lockSkewingY = true;
    canvas.add(silencePath);
    canvas.renderAll();
};
/**
 * Created by khoi on 10/29/16.
 */
